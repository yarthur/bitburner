import * as hackableServers from "/utils/hackableServers.js";
/** @param {NS} ns **/
export async function main(ns) {
	await hackableServers.clear(ns);

	ns.spawn("coordinate.js", 1);
};