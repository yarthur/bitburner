/** @param {NS} ns **/
export async function main(ns) {
	const [
		target,
		securityThreshold,
		moneyThreshold,
		maxMemory,
		allocatedMemory,
		weakenMemory,
		growMemory
	] = ns.args;

	ns.disableLog("ALL");

	while (true) {
		if (maxMemory - ns.getServerUsedRam("home") >= allocatedMemory) {
			const currentSecurity = ns.getServerSecurityLevel(target);
			const moneyAvailable = ns.getServerMoneyAvailable(target);
	
			ns.print(`${target} Stats
	Security: ${currentSecurity} < ${securityThreshold}
	Money: ${ns.nFormat(moneyAvailable, "0,0")} > ${ns.nFormat(moneyThreshold, "0,0")}`);
	
			if (currentSecurity > securityThreshold) {
				ns.print(`Weakening ${target}`);
				await ns.run(`/attack/${target}/weaken.js`, allocatedMemory / weakenMemory);
			} else if (moneyAvailable < moneyThreshold) {
				ns.print(`Growing ${target}`);
				await ns.run(`/attack/${target}/grow.js`, allocatedMemory / growMemory);
			} else {
				ns.spawn("launchCoordinatedAttack.js", 1, target);
			}
		}

		await ns.sleep(500);
	}
};