import gainAdmin from "util-gainAdmin.js";
import getServersWithAdmin from "util-getServersWithAdmin.js";

/** @param {NS} ns **/
export async function main(ns) {
	const host = ns.getHostname();
	const target = ns.args[0] || host;
	const script = ["launchAttack.js", "attack.js"];

	gainAdmin(ns);

	const servers = getServersWithAdmin(ns);

	for(const { hostname } of servers) {
		if (ns.getServerMaxRam(hostname) > 0 && hostname !== "home") {
			await ns.killall(hostname);
			await ns.scp(script, host, hostname);
			await ns.exec(script[0], hostname, 1, target);
		}
	}
}