/** @param {NS} ns **/
export async function main(ns) {
	const host = ns.getHostname();
	const target = ns.args[0] || host;
	const script = "attack.js";
	const memoryRequired = ns.getScriptRam(script);
	const threadCount = Math.floor(ns.getServerMaxRam(host) / memoryRequired);	
	const securityThreshold = ns.getServerMinSecurityLevel(target) + 2;
	const moneyThreshold = ns.getServerMaxMoney(target) * .85;

	if (threadCount > 0) {
		ns.spawn(script, threadCount, target, securityThreshold, moneyThreshold);
	}
}