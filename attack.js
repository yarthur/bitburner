/** @param {NS} ns **/
export async function main(ns) {
	const [target, securityThreshold, moneyThreshold] = ns.args;

	while (true) {
		const currentSecurity = ns.getServerSecurityLevel(target);
		const moneyAvailable = ns.getServerMoneyAvailable(target);

		ns.print(`${target} Stats
Security: ${currentSecurity} < ${securityThreshold}
Money: ${ns.nFormat(moneyAvailable, "0,0")} > ${ns.nFormat(moneyThreshold, "0,0")}`);

		if (currentSecurity > securityThreshold) {
			await ns.weaken(target);
		} else if (moneyAvailable < moneyThreshold) {
			await ns.grow(target);
		} else {
			await ns.hack(target);
		}
	}
};