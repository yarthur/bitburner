import * as hackableServers from "/utils/hackableServers.js";

const canPurchase = (ns) => {
	const moneyAvailable = ns.getPlayer().money;
	const serverCost = ns.getPurchasedServerCost(ns.getPurchasedServerMaxRam());
	ns.print(`${moneyAvailable} > ${serverCost}`);

	return moneyAvailable > serverCost;
}

/** @param {NS} ns **/
export async function main(ns) {
	const servers = ns.getPurchasedServers();
	const ram = ns.getPurchasedServerMaxRam();
	const host = ns.getHostname();
	const attackScript = "attack.js";

	while (
		servers.length < ns.getPurchasedServerLimit()
		&& canPurchase(ns)
	) {
		const newServer = ns.purchaseServer(
			`pserve-${ns.nFormat(servers.length, "00")}`,
			ram
		);

		servers.push(newServer);	
	}

	const targets = await hackableServers.get(ns);

	const memoryPerTarget = ram / targets.length;

	for (const server of servers) {
		await ns.killall(server);
		await ns.sleep(500);
		for (const target of targets) {
			const scriptsPath = `/attack/${target}`;

			const scripts = ns.ls(host, scriptsPath);
			ns.print(scriptsPath);
			ns.print(scripts);
			ns.print(memoryPerTarget)

			await ns.scp(scripts, host, server);
			await ns.exec(`${scriptsPath}/${attackScript}`, server, 1, memoryPerTarget);	
			await ns.sleep(10);
		}
	}

	ns.spawn("/coordinateAttack.js", 1);
}