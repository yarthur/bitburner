import gainAdmin from "/utils/gainAdmin.js";
import buildAttackScripts from "/utils/buildAttackScripts.js";
import getRankedServers from "/utils/getRankedServers.js";
import hackableServers from "/utils/hackableServers.js"; 

/** @param {NS} ns **/
export async function main(ns) {
	await gainAdmin(ns);
	await buildAttackScripts(ns);
	await ns.sleep(1000);
	
	const servers = getRankedServers(ns);
	const serversToAvoid = await hackableServers(ns);
	const script = "prepareForLaunch.js";

	// Reserve some ram to run arbitrary scripts.
	const reservedRam = 16
	const allocatedMemory = Math.floor(ns.getServerMaxRam(ns.getHostname()) - ns.getScriptRam(script) - reservedRam);

	for(const server of servers) {
		if (serversToAvoid.includes(server.hostname) === false) {
			const securityThreshold = server.minDifficulty + 2;
			const moneyThreshold = server.moneyMax * .85;
			const scriptPath = `/attack/${server.hostname}`;
			const weakenMemory = ns.getScriptRam(`${scriptPath}/weaken.js`);
			const growMemory = ns.getScriptRam(`${scriptPath}/grow.js`);
			
			ns.spawn(script, 1, server.hostname, securityThreshold, moneyThreshold, ns.getServerMaxRam(ns.getHostname()), allocatedMemory, weakenMemory, growMemory);
		}
	}
}