import gainAdmin from "/utils/gainAdmin.js";
import buildAttackScripts from "/utils/buildAttackScripts.js";
import getServersWithAdmin from "/utils/getServersWithAdmin.js";
import * as hackableServers from "/utils/hackableServers.js";

/** @param {NS} ns **/
export async function main(ns) {
	await gainAdmin(ns);
	await buildAttackScripts(ns);

	const host = ns.getHostname();
	const newTarget = ns.args[0];
	const servers = getServersWithAdmin(ns);
	const purchasedServers = ns.getPurchasedServers();

	await hackableServers.add(ns, newTarget);
	const targets = await hackableServers.get(ns);

	let targetCount = 0;
	for(const { hostname } of servers) {
		if (
			ns.getServerMaxRam(hostname) > 0
			&& hostname !== "home"
			&& purchasedServers.includes(hostname) === false
		) {
			let target = hostname;
			if (targets.includes(hostname) === false) {
				target = targets[targetCount % targets.length];
				targetCount += 1;
			}

			const scriptPath = `/attack/${target}`;		
			const scripts = ns.ls(host, scriptPath);

			await ns.killall(hostname);
			await ns.scp(scripts, host, hostname);
			await ns.exec(`${scriptPath}/attack.js`, hostname, 1);
		}
	}

	if (ns.getServerMaxRam(host) > ns.getScriptRam("/managePurchasedServers.js")) {
		ns.spawn("/managePurchasedServers.js", 1);
	}
	
	ns.spawn("/coordinateAttack.js", 1);
}