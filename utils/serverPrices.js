/** @param {NS} ns **/
export async function main(ns) {
	const maxRam = ns.getPurchasedServerMaxRam();

	for (let ram = 1; ram <= maxRam; ram *= 2) {
		ns.tprint(`${ns.nFormat(ram, "b")}: ${ns.nFormat(ns.getPurchasedServerCost(ram), "0,0")}`);
	}
}