import getTargetableServers from "/utils/getTargetableServers.js";

const rankServers = (ns) => {
	const servers = getTargetableServers(ns);	
	return servers.map((server) => {
		server.moneyOverMinSecurity = server.moneyMax / server.minDifficulty;

		return server; 
	}).sort((a, b) => {
		if (a.requiredHackingSkill === b.requiredHackingSkill) {
			return a.moneyOverMinSecurity - b.moneyOverMinSecurity;
		}

 		return a.requiredHackingSkill - b.requiredHackingSkill;
	});
}

/** @param {NS} ns **/
export async function main(ns) {
	const servers = rankServers(ns);
	const currencyFormat = "$0.000a"

	const output = servers.map((server) => {
		return `${server.hostname}:
	${ns.nFormat(server.requiredHackingSkill, "0,0")}
	${ns.nFormat(server.moneyOverMinSecurity, currencyFormat)} (${ns.nFormat(server.moneyMax, currencyFormat)} / ${server.minDifficulty})
`;
	});

	ns.tprint(`************
Ranked Servers:
${output.join("\n")}`);

//	return servers;
};

export default rankServers;