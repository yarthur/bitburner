import getServers from "/utils/getServers.js";

const getServersWithAdmin = (ns) => {
	const servers = getServers(ns);

	return servers.filter((server) => {
		return server.hasAdminRights;
	});
};

/** @param {NS} ns **/
export async function main(ns) {
	const servers = getServersWithAdmin(ns);
	let output = "";

	servers.forEach((server) => {
		output += `************
${server.hostname}
Has Admin: ${server.hasAdminRights}
Max Money: ${ns.nFormat(server.moneyMax, "0,0")}
Min Security: ${server.minDifficulty}
Required Level: ${server.requiredHackingSkill}
Cores: ${server.cpuCores}
`;
	})

	ns.tprint(`${servers.length} Available Servers With Admin Rights:
${output}`);
};

export default getServersWithAdmin;