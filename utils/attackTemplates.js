export const hack = (ns, target) => {
	return `/** @param {NS} ns **/
export async function main(ns) {
	return await ns.hack("${target.hostname}");
};`;
};

export const grow = (ns,target) => {
	return `/** @param {NS} ns **/
export async function main(ns) {
	return await ns.grow("${target.hostname}");
};`;
};

export const weaken = (ns, target) => {
	return `/** @param {NS} ns **/
export async function main(ns) {
	return await ns.weaken("${target.hostname}");
};`;
};

export const attack = (ns, target, weakenMem, growMem, hackMem, attackMem) => {
	const securityThreshold = target.minDifficulty + 2;
	const moneyThreshold = target.moneyMax * 0.85;
	const targetName = target.hostname;

	return `/** @param {NS} ns **/
export async function main(ns) {
	ns.disableLog("ALL");
	
	await ns.sleep(10000);
	const target = "${target.hostname}"
	const securityThreshold = ${securityThreshold};
	const moneyThreshold = ${moneyThreshold};
	const host = ns.getHostname();
	const memoryAllotted = ns.args[0] || ns.getServerMaxRam(host);
	const adjustedMemoryAllotted = memoryAllotted - ${attackMem};
	const currencyFormat = "$0.000a";

	while (true) {
		const memoryFree = ns.getServerMaxRam(host) - ns.getServerUsedRam(host);

		if (memoryFree >= adjustedMemoryAllotted) {
			const currentSecurity = ns.getServerSecurityLevel(target);
			const moneyAvailable = ns.getServerMoneyAvailable(target);
	
			ns.print(\`\${target} Stats'
	Security: \${currentSecurity} < \${securityThreshold}'
	Money: \${ns.nFormat(moneyAvailable, currencyFormat)} > \${ns.nFormat(moneyThreshold, currencyFormat)}
\`);

			if (currentSecurity > securityThreshold) {
				ns.print("Weakening ${targetName}.");
				await ns.run("/attack/${targetName}/weaken.js", adjustedMemoryAllotted / ${weakenMem});
			} else if (moneyAvailable < moneyThreshold) {
				ns.print("Growing ${targetName}.");
				await ns.run("/attack/${targetName}/grow.js", adjustedMemoryAllotted / ${growMem});
			} else {
				ns.print("Hacking ${targetName}.");
				await ns.run("/attack/${targetName}/hack.js", adjustedMemoryAllotted / ${hackMem});
			}
		}

		await ns.sleep(50);
	}
};`
};

/** @param {NS} ns **/
export async function main(ns) {

};