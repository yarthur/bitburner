import getServers from "/utils/getServers.js";

const gainAdmin = async (ns) => {
	const crackingScripts = {
		"BruteSSH.exe": ns.brutessh,
		"FTPCrack.exe": ns.ftpcrack,
		"relaySMTP.exe": ns.relaysmtp,
		"HTTPWorm.exe": ns.httpworm,
		"SQLInject.exe": ns.sqlinject
	};
	
	const servers = await getServers(ns);

	for (const server of servers) {
		const serverName = server.hostname;
		let adminRights = server.hasAdminRights;

		if (!adminRights) {
			Object.entries(crackingScripts).forEach(([exe, script]) => {
				if (ns.fileExists(exe, "home")) {
					script(serverName);
				}
			});

			try {	
				ns.nuke(serverName);
				ns.tprint(`${serverName} successfully nuked!`);
				adminRights = true;
			}
			catch(e) {}
		}
/*
		if (
			ns.fileExists("sourceFile4")
			&& adminRights
			&& server.backdoorInstalled === false
		) {
			ns.tprint("Backdoor for ${serverName}?");
			try {
				await ns.installBackdoor()
				ns.tprint(`Backdoor successfully installed on ${serverName}!`);
			} catch (e) {
				
			}
			
			await ns.sleep(1000);
		}
*/
	}
};

/** @param {NS} ns **/
export async function main(ns) {
	await gainAdmin(ns);

};

export default gainAdmin;