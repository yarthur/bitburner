import * as hackableServers from "/utils/hackableServers.js";

const canPurchase = (ns) => {
	const moneyAvailable = ns.getPlayer().money;
	const serverCost = ns.getPurchasedServerCost(ns.getPurchasedServerMaxRam());

	return moneyAvailable > serverCost;
}

const getTargetScripts = async (ns) => {
	const targets = await hackableServers.get(ns);

	return targets.map((target) => {
		return `/attack/${target}.js`;
	})
};

/** @param {NS} ns **/
export async function main(ns) {
	const servers = ns.getPurchasedServers();
	const ram = ns.getPurchasedServerMaxRam();
	const attackScript = "attack.js";

	if (
		servers.length > ns.getPurchasedServerLimit()
		&& canPurchase(ns)
	) {
		const newServer = ns.purchaseServer(
			`pserve-${ns.nFormat(servers.length, "00")}`,
			ram
		);

		servers.push(newServer);	
	}

	const targetScripts = await getTargets(ns);
	const threadsAvailable = ram / ns.getScriptRam(targetScripts[0]);
	const threadCount = threadsAvailable/targetScripts.length;

	for (const server of servers) {
		await ns.killall();
		await ns.sleep(500);

		for (const script of targetScripts) {
			await ns.scp(script, server);
			await ns.exec(script, server, threadCount);	
			await ns.sleep(10);
		}
	}
}