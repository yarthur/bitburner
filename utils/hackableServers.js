const hackableFile = "/data/hackableServers.txt";
const delimiter = ",";

const getHackable = async (ns) => {
	const serversString = await ns.read(hackableFile);
	return new Set(serversString.split(delimiter))
};

const writeHackable = async (ns, hackable) => {
	hackable.delete("");

	const hackableString = [...hackable].join(delimiter);

	return await ns.write(hackableFile, hackableString, "w");	
}

export const get = async (ns) => {
	const hackable = await getHackable(ns);
	return [...hackable];
};

export const add = async (ns, server) => {
	const hackable = await getHackable(ns);

	hackable.add(server);

	return await writeHackable(ns, hackable);
};

export const del = async (ns, server) => {
	const hackables = await getHackable(ns);

	hackable.delete(server);

	return await writeHackable(ns, hackable);
}

export const clear = async (ns) => {
	return await writeHackable(ns, new Set());
};

/** @param {NS} ns **/
export async function main(ns) {
	const hackableServers = await get(ns);

	ns.tprint(`************
${hackableServers.length} Hackable Servers:
   ${hackableServers.join("\n   ")}`);
};

export default get;