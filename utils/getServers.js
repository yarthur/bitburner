const gatherNames = (ns, names = [], host = null) => {
	if (host === null) {
		host = ns.getHostname();
	}

	return ns.scan(host)
		.reduce((nameSet, serverName) => {
			if (nameSet.includes(serverName)) {
				return nameSet;
			}

			nameSet.push(serverName);

			return gatherNames(ns, nameSet, serverName);
		}, names);
};

const getServers = (ns, servers = [], host = null) => {
	const serverNames = gatherNames(ns);

	return serverNames.map((server) => {
		return ns.getServer(server);
	});
};

/** @param {NS} ns **/
export async function main(ns) {
	const servers = getServers(ns);

	const output = servers.map((server) => {
		return `************
${server.hostname}
Has Admin: ${server.hasAdminRights}
Max Money: ${ns.nFormat(server.moneyMax, "0,0")}
Min Security: ${server.minDifficulty}
Required Level: ${server.requiredHackingSkill}
Cores: ${server.cpuCores}
`;
	})
	ns.tprint(`${servers.length} Available Servers:
   ${output.join("\n   ")}`);
}

export default getServers;