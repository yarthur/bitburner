import getServers from "util-getServers.js";

const getServerList = (ns) => {
	const servers = getServers(ns);

	return servers.filter((server) => {
		return server.hostname !== "home"
			&& server.purchasedByPlayer === false
			&& server.hasAdminRights
			&& server.backdoorInstalled === false;
	});
}

/** @param {NS} ns **/
export async function main(ns) {
	const [servers] = ns.args;

	if (servers === null) {
		servers = getServerList(ns);
	} else {
		servers = servers.split(",");
	}
	
	servers = servers.split(",");

	if (servers.length === 0) {
		ns.alert(`installBackdoor complete.`);
		ns.exit();
	}

	await ns.installBackdoor();
	ns.spawn("util-installBackdoor.js", 1, servers.join(","));
}