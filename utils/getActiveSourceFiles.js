/** @param {NS} ns 
 * Get a dictionary of active source files, taking into account the current active bitnode as well. **/
const getActiveSourceFiles = (ns, fnGetNsDataThroughFile, includeLevelsFromCurrentBitnode = true) => {
    checkNsInstance(ns, '"getActiveSourceFiles"');
    let dictSourceFiles
    // Find out what source files the user has unlocked
    try {
		dictSourceFiles = ns.getOwnedSourceFiles()
			.map((sf) => {
				return [sf.n, sf.lvl];
			})
	} catch {}

    // If the user is currently in a given bitnode, they will have its features unlocked
    if (includeLevelsFromCurrentBitnode) {
        const bitNodeN = (await fnGetNsDataThroughFile(ns, 'ns.getPlayer()', '/Temp/player-info.txt')).bitNodeN;
		
        dictSourceFiles[bitNodeN] = Math.max(3, dictSourceFiles[bitNodeN] || 0);
    }

    return dictSourceFiles;
}

/** @param {NS} ns **/
export const main = async (ns) => {
	const activeSourceFiles = getActiveSourceFiles(ns, true);

	const output = getActiveSourceFiles.map(([fileName, fileLevel]) => {
		return `${fileName}: ${fileLevel}`;
	});

	ns.tprint(`************
Active Source Files:
${output.join("\n")}`);

	return getActiveSourceFiles;
};

export default getActiveSourceFiles;
