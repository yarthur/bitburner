import getTargetableServers from "/utils/getTargetableServers.js";
import * as attackTemplates from "/utils/attackTemplates.js";

const getScriptStats = async (ns, scripts) => {
	const stats = [];

	for (const script of scripts) {
		const scriptName = `/tmp/${script}.js`;
		const scriptContents = attackTemplates[script](ns, ns.getServer("home"), 0, 0, 0, 0, 0);
		await ns.write(scriptName, scriptContents, "w");
		stats.push(ns.getScriptRam(scriptName));
		await ns.rm(scriptName);
	}

	return stats;
}

const buildAttackScripts = async (ns) => {
	const servers = await getTargetableServers(ns);
	const scripts = ["weaken", "grow", "hack", "attack"];
	const scriptStats = await getScriptStats(ns, scripts);

	for (const server of servers) {
		for (const script of scripts) {
			const attackScriptName = `/attack/${server.hostname}/${script}.js`;
			const scriptContents = attackTemplates[script](ns, server, ...scriptStats);
	
			await ns.write(attackScriptName, scriptContents, "w");
			await ns.sleep(10);
			ns.print(`${attackScriptName} written.`);
		}
	}
};

/** @param {NS} ns **/
export async function main(ns) {
	return await buildAttackScripts(ns);
};

export default buildAttackScripts;