import getServersWithAdmin from "/utils/getServersWithAdmin.js";

const getTargetableServers = (ns) => {
	const servers = getServersWithAdmin(ns);

	return servers.filter((server) => {
		return server.moneyMax > 0;
	});
}
/** @param {NS} ns **/
export async function main(ns) {
	const servers = getTargetableServers(ns);

	const output = servers.map((server) => {
		return `${server.hostname}: ${ns.nFormat(server.moneyMax, "$0.000a")}`;
	});

	ns.tprint(`************
${servers.length} Targetable Servers:
   ${output.join("\n   ")}`);

   return servers;
};

export default getTargetableServers;